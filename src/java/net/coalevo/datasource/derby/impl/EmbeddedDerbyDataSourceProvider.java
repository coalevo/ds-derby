/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.datasource.derby.impl;

import net.coalevo.datasource.model.DataSourceProvider;

import javax.sql.DataSource;

import org.apache.derby.jdbc.EmbeddedDataSource;
import org.osgi.framework.BundleContext;

import java.io.File;

/**
 * This class implements a {@link DataSourceProvider} for
 * an embedded derby datasource. This datasource is configured
 * automagically, and will store in this bundle's data directory
 * within the container.
 * <p/>
 * This implementation may provide simple configuration and low maintenance
 * storage for development and small site setups, out-of-the -box.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EmbeddedDerbyDataSourceProvider
    implements DataSourceProvider {

  private EmbeddedDataSource m_DataSource;

  public EmbeddedDerbyDataSourceProvider() {
  }//constructor

  public void activate(BundleContext bc) {
    //1. check directory
    File f = bc.getDataFile("");
    File dir = new File(f, "embedded_store");
    boolean createrequired = !dir.exists();
    //2. Prepare datasource
    m_DataSource = new EmbeddedDataSource();
    if(createrequired) {
      m_DataSource.setCreateDatabase("create");
    }
    m_DataSource.setDataSourceName(getIdentifier());
    m_DataSource.setDatabaseName(dir.getAbsolutePath());
  }//activate

  public void deactivate() {
    m_DataSource = null;
  }//deactivate

  public String getIdentifier() {
    return "derby";
  }//getIdentifier

  public DataSource getDataSource() {
    return m_DataSource;
  }//getDataSource

}//class EmbeddedDerbyDataSourceProvider
